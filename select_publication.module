<?php

/**
 * Implementation of hook_form_alter().
 */
function select_publication_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'node_type_form') {
    _select_publication_node_type_form_alter($form, $form_state, $form_id);
  } else if (isset($form['type']) && $form['type']['#value'].'_node_form' == $form_id) {
    _select_publication_node_form_alter($form, $form_state, $form_id);
  }
}

/**
 * Implementation of hook_form_alter for node_type_form forms
 */
function _select_publication_node_type_form_alter(&$form, $form_state, $form_id) {
  $form['submission']['select_publication'] = array(
    '#type' => 'checkbox',
    '#title' => t('Select publication'),
    '#return_value' => 1,
    '#default_value' => variable_get('select_publication_'. $form['#node_type']->type, false),
    '#description' => t('Enable users to select publications when adding/editing nodes of this type'),
  );
}

/**
 * Implementation of hook_form_alter for node forms
 */
function _select_publication_node_form_alter(&$form, $form_state, $form_id) {
  if (!variable_get('select_publication_'.$form['type']['#value'], false)) {
    return;
  }
  
  $node = $form['#node'];
  
  // Get this nodes' list of publications/editions
  $current_publications = array();
  if ($node->nid) {
    $result = db_query("SELECT eid FROM {epublish_edition_node} WHERE nid=%d", $node->nid);
    while ($eid = db_fetch_object($result)) {
      $current_publications[] = $eid->eid;
    }
  }
  
  // Get the list of all publications/editions
  $all = array();
  $order = variable_get('epublish_edition_order', 'DESC');
  $publications = db_query("SELECT pid, name FROM {epublish_publication}");
  while ($pub = db_fetch_object($publications)) {
    $editions = db_query("
        SELECT *
          FROM {epublish_edition} 
         WHERE pid='%d' 
      ORDER BY volume $order, number $order, pubdate $order, eid $order",
      $pub->pid
    );
    
    while ($edition = db_fetch_object($editions)) {
        $all[t($pub->name)][$edition->eid] = theme('epublish_edition_reference', $edition);
    }
  }
  
  // And create the form item
  $form['select_publication'] = array(
    '#type' => 'select',
    '#title' => t('Publications'),
    '#options' => $all,
    '#default_value' => $current_publications,
    '#multiple' => true,
    '#description' => '',
  );
}
 
/**
 * Implementation of hook_nodeapi().
 */
function select_publication_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  if ($op != 'insert' && $op != 'update') {
    return;
  }
  if (!array_key_exists('select_publication', $node) ||
      !variable_get('select_publication_'.$node->type, false)) {
    return;
  }
  
  $select_publication = $node->select_publication;
  
  // Get the list of current editions
  $editions = db_query("
    SELECT *
      FROM {epublish_edition_node}
     WHERE nid = '%d'",
     $node->nid
   );

  // Work out which ones to add/remove
  $to_remove = array();
  while ($edition = db_fetch_object($editions)) {
    if (!in_array($edition->eid, $select_publication)) {
      $to_remove[] = $edition->eid;
    } else {
      unset($select_publication[$edition->eid]);
    }
  }
  
  
  // Remove editions
  if (count($to_remove)) {
    db_query("
      DELETE FROM {epublish_edition_node}
            WHERE nid='%d' 
              AND eid IN (".implode(',',$to_remove).")",
      $node->nid
    );
  }
  
  // Add editions
  foreach ($select_publication as $eid) {
    db_query("
      INSERT INTO {epublish_edition_node} (eid, nid)
           VALUES('%d', '%d')",
      $eid, $node->nid
    );
  }
}

function select_publication_views_api() {
  return array(
    'api' => 2,
  );
}

