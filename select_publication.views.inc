<?php

function select_publication_views_data() {
  $data['epublish_edition_node'] = array(
    'table' => array(
      'group' => 'EPublish',
      'title' => 'EPublish Node',
      'join' => array(
        'node' => array(
          'left_field' => 'nid',
          'field' => 'nid',        
         ),
      ),
    )
  );
  
  $data['epublish_publication'] = array(
    'table' => array(
      'group' => 'EPublish',
      'title' => 'EPublish Publication',
      'join' => array(
				'node' => array(
					'left_table' => 'epublish_edition',
					'left_field' => 'pid',
					'field' => 'pid',
				),
      ),
    )
  );
  
  $data['epublish_edition'] = array(
    'table' => array(
      'group' => 'EPublish',
      'title' => 'EPublish Publication',
      'join' => array(
				'epublish_edition_node' => array(
					'left_field' => 'eid',
					'field' => 'eid',        
				),
				'epublish_publication' => array(
					'left_field' => 'pid',
					'field' => 'pid',
				),
				'node' => array(
					'left_table' => 'epublish_edition_node',
					'left_field' => 'eid',
					'field' => 'eid',
				),
      ),
    )
  );

  
  // fields
  $data['epublish_edition']['pid'] = array(
    'title' => t('Publication ID'),
    'help' => t('The EPublish Publication ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
  	)
	);

	$data['epublish_edition']['eid'] = array(
    'title' => t('Edition ID'),
    'help' => t('The EPublish Edition ID.'), // The help that appears on the UI,
    // Information for displaying the nid
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    )
  );
  
  $data['epublish_publication']['current_eid'] = array(
    'title' => t('Current Edition ID'),
    'help' => t('The current EPublish Edition ID of the associated publication.'), // The help that appears on the UI,
    // Information for displaying the nid
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    )
  );

  return $data;  
}